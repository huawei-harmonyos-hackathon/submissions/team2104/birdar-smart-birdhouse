/*Birdar v.3
 * 
 * eng.Toni Marinov 
 * dimmar1993@gmail.com
 * HarmonyOS Hackaton 19.11.2021
 * Edited a simple GUI for Android using BlueDisplay
 * 
 * Added servo control for the feeder
 * 
 * Added a button to open and close feeder
 * 
 * Added a transistor controlled relay to switch a IR camera remote button, without ruining the remote. 
 * 
 * Added second GUI button for manual photo trigger
 * 
 * Added a blue circle which stays on the display if the US has been triggered
 *
 * 
 * added 
 * BlueDisplayBlink.cpp
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

#include <Arduino.h>

#include <Ultrasonic.h>
#include <Servo.h>
unsigned long onehour = 6*1000UL;
unsigned long oneminute = 60*1000UL;
unsigned long eighthours = onehour*8UL;
Servo myservo;

Ultrasonic ultrasonic(7, 8);
int distance;

#include "BlueDisplay.h"

/****************************************************************************
 * Change this if you have reprogrammed the hc05 module for other baud rate
 ***************************************************************************/
#ifndef BLUETOOTH_BAUD_RATE
//#define BLUETOOTH_BAUD_RATE BAUD_115200
#define BLUETOOTH_BAUD_RATE BAUD_9600
#endif

#define DISPLAY_WIDTH  DISPLAY_HALF_VGA_WIDTH  // 320
#define DISPLAY_HEIGHT 600 // 240

bool doBlink = true;

/*
 * The Start Stop button
 */
BDButton TouchButtonBlinkStartStop;
BDButton TouchButtonPhoto;
// Touch handler for buttons
void doBlinkStartStop(BDButton * aTheTochedButton, int16_t aValue);
void doPhoto(BDButton * atTheTochetButton);
// Callback handler for (re)connect and resize
void initDisplay(void);
void drawGui(void);

/*******************************************************************************************
 * Program code starts here
 *******************************************************************************************/

void setup() {
    // Initialize the LED pin as output.
    pinMode(LED_BUILTIN, OUTPUT);    
    pinMode(51, OUTPUT);
    Serial.begin(9600);
    myservo.attach(9);
    myservo.write(180);


    initSerial(BLUETOOTH_BAUD_RATE);


    // Register callback handler and check for connection
    BlueDisplay1.initCommunication(&initDisplay, &drawGui);

#if defined (USE_SERIAL1) // defined in BlueSerial.h
// Serial(0) is available for Serial.print output.
#  if defined(SERIAL_USB)
    delay(2000); // To be able to connect Serial monitor after reset and before first printout
#  endif
// Just to know which program is running on my Arduino
    Serial.println(F("START " __FILE__ " from " __DATE__ "\r\nUsing library version " VERSION_BLUE_DISPLAY));
#else
    BlueDisplay1.debug("START " __FILE__ " from " __DATE__ "\r\nUsing library version " VERSION_BLUE_DISPLAY);
#endif

}

void loop() {
    // Pass INC as a parameter to get the distance in inches
  distance = ultrasonic.read();
  
  Serial.print("Distance in CM: ");
  if (distance <= 9) {
    Serial.print("Bird!");
    BlueDisplay1.debug("\r\nBird!");
    BlueDisplay1.fillCircle(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 40, COLOR_BLUE);
    digitalWrite(51,HIGH);
    delay(400);
    digitalWrite(51,LOW);
//    BlueDisplay1.drawRect(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2, 40, COLOR_BLUE);
    }
  Serial.println(distance);
  delay(1000);
  onehour = onehour - 1000;
  Serial.print("OneHour =");
  Serial.print(onehour);
  if (onehour <= 1) {
    onehour = 60*60*1000UL;
    myservo.write(160);
    delay(200);
    myservo.write(180);
  }

    // To get blink enable event
    checkAndHandleEvents();
}

/*
 * Function used as callback handler for connect too
 */
void initDisplay(void) {
    // Initialize display size and flags
    BlueDisplay1.setFlagsAndSize(BD_FLAG_FIRST_RESET_ALL | BD_FLAG_USE_MAX_SIZE | BD_FLAG_TOUCH_BASIC_DISABLE, DISPLAY_WIDTH,
    DISPLAY_HEIGHT);
    // Initialize button position, size, colors etc.
    TouchButtonBlinkStartStop.init((DISPLAY_WIDTH - BUTTON_WIDTH_2) / 2, BUTTON_HEIGHT_4_LINE_4, BUTTON_WIDTH_2,
    BUTTON_HEIGHT_4, COLOR_BLUE, "FEED", 44, FLAG_BUTTON_DO_BEEP_ON_TOUCH | FLAG_BUTTON_TYPE_TOGGLE_RED_GREEN, doBlink,
            &doBlinkStartStop);
    TouchButtonBlinkStartStop.setCaptionForValueTrue("FEED");
    TouchButtonPhoto.init((DISPLAY_WIDTH - BUTTON_WIDTH_2) / 2, BUTTON_HEIGHT_4_LINE_2, BUTTON_WIDTH_2,
    BUTTON_HEIGHT_4, COLOR_PURPLE, "PHOTO", 44, FLAG_BUTTON_DO_BEEP_ON_TOUCH | FLAG_BUTTON_TYPE_TOGGLE_RED_GREEN, doBlink,
            &doOpenCloseFeeder);
    
}

/*
 * Function is called for resize + connect too
 */
void drawGui(void) {
    BlueDisplay1.clearDisplay(COLOR_YELLOW);
    TouchButtonBlinkStartStop.drawButton();
    TouchButtonPhoto.drawButton();
}

/*
 * Change doBlink flag as well as color and caption of the button.
 */
void doBlinkStartStop(BDButton * aTheTouchedButton __attribute__((unused)), int16_t aValue) {
    
    myservo.write(160);
    delay(200);
    myservo.write(180);


    // * This debug output can also be recognized at the Arduino Serial Monitor
     
    BlueDisplay1.debug("FEEDING TIME"); //displays on lower part of the Android display
}

void doOpenCloseFeeder(BDButton * aTheTouchedButton __attribute__((unused)), int16_t aValue) {
    
    digitalWrite(51,HIGH); //Relay trigger for IR remote
    delay(200);
    digitalWrite(51,LOW);

    // * This debug output can also be recognized at the Arduino Serial Monitor
     
    BlueDisplay1.debug("PHOTO TIME");
}
