# BirDar - smart birdhouse

###################################
#               BirDar            #
#       HarmonyOS challenge       #
#       Team21                    #
#       eng.Toni Marinov          #
#           /\                    #
#          /  \                   #   
#         / ,_ \                  #  
#        | >' ) |                 #  
#        | ( ( \|                 #  
#        |  ''|\|   Art by Morfina#
###################################

PROJECT
BirDar is a smart birdhouse notifier that will help your kids notice birds on your window with a special notification.
It also has an integrated automated feeder and IR remote trigger for a camera.

Birdar uses Arduino Mega as a MCU, UHC-04 Ultrasound sensor, HC-05 Bluetooth module, Servo SG90 and Sony IR remote control. It's powered with a refurbished 220V to 12V/3A board from a LEDTV and a voltage regulator LM2596 to get a stable 5V from the 12V. For triggering of the remote control I used a 5V Relay, controlled by an npn transistor, connected to one of the Arduino pins.

For the application part, you have to install BlueDisplay and pair with the HC-05 module. The Arduino will then draw the GUI directly. After that you'll need Toast Catcher app to catch all the toast notifications coming from BlueDisplay. Then an action from Toast app that looks for "Bird!" should filter all the unnecesarry other toast messages. From there you have escaped the Blue Display app. You can now install MacroDroid and use the push notificaion of "Bird!" as a trigger and set your own automation action. Mine is blinking flashlight of the phone 3 times and text so speech "Okay Google, broadcast Bird Outside". Broadcasting doesn't work so far.

Looking for a way to implement an action with Home Assistant or openHAB

Dead ends so far:
Home assistant on Orange Pi 2g IOT:
The board is not ok as a whole. It lacks the promised WiringOP (Wiring Pi for orange Pi) support and the developers didn't implement it. The ubuntu image for it drops network constantly. The Debian has other problems with the wifi. The power supply is really sensitive.

Home assistant on VM: Works but isn't accesible from time to time. Connected to my phone and got some sensor data, but finding another workaround with a 4th app just to get to home assistant is just too much. Cleaner way will be with a RPI and upload HASSIO there while receiving data from bluetooth or esp32 wifi.




